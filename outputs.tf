output "cluster_certificate_authority_data" {
  description = "Certificate Authority Data do cluster Kubernetes."
  value       = base64decode(aws_eks_cluster.this.certificate_authority[0].data)
}

output "cluster_endpoint" {
  description = "Endpoint da API do cluster Kubernetes."
  value       = aws_eks_cluster.this.endpoint
}

output "kubeconfig_token" {
  description = "Cluster token."
  value       = data.aws_eks_cluster_auth.cluster.token
}
output "workers_arn" {
  description = "Arn Workers Nodes."
  value       = aws_iam_role.workers.arn
}
