terraform {
  required_version = ">= 0.13.1"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.73.0"
    }
    http = {
      source  = "terraform-aws-modules/http"
      version = "~> 2.4.0"
    }
  }
}
