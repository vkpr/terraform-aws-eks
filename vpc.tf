module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = ">= 3.2.0"

  name                 = var.vpc_name
  cidr                 = var.cidr_block
  azs                  = try(var.aws_availability_zones, data.aws_availability_zones.available.names)
  public_subnets       = var.public_subnets
  private_subnets      = var.private_subnets
  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true

  public_subnet_tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
  }

  map_public_ip_on_launch = false
}
